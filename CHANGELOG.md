# Changelog

#### v1.0.5

- Adapt to mTask-server 0.6

#### v1.0.4

- Adapt to mTask 0.3

#### v1.0.3

- expand debug application
- reinstate blink example
- changelog

#### v1.0.2

- license
- readme
- new mTask dependencies

#### v1.0.1

- update to new gpio and liftsds interface

## v1.0

- initial version
