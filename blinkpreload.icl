module blinkpreload

import StdEnv, iTasks
import Data.Functor
import Data.Func
import System.GetOpt
import System.File
import qualified Data.Map as DM

import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasksWithOptions (
	withDefaultEngineCLIOptions
		[ Option [] ["preload"] (ReqArg (fmap o preloadOpt) "FILE")
			"Output the preloaded_tasks.h file to FILE"
		] positionals
		startable
		(flip (+++) " [OPTIONS]")
	)
	w
where
	preloadOpt a s = {s & userOpts='DM'.put "preload" [a] s.userOpts}

	positionals [] = Ok id
	positionals _ = Error ["Positional arguments not supported"]

	startable eo = Ok $ case 'DM'.get "preload" eo.userOpts of
		?None = onRequest "/" main
		?Just [fp] = onStartup $ preload fp

preload :: String -> Task ()
preload fp
	=    preloadTasks [MTB blink, MTB r42]
	>>~ \f->accWorldError (writeFile fp f) toString

main :: Task Bool
main = updateInformation [] {host="localhost",port=8123,pingTimeout= ?None}
	>>? \spec->withDevice spec
		\dev->liftmTask blink dev
			-|| (liftmTask r42 dev >>- viewInformation [])

blink :: Main (MTask v Bool) | mtask v
blink
	// Builtin LED is D4 on the D1 Wemos Mini
	= declarePin D4 PMOutput \d4->
	fun \blink=(\x->
		     delay (lit 500)
		>>|. writeD d4 x
		>>=. blink o Not)
	In {main=blink (lit True)}

r42 :: Main (MTask v Int) | mtask v
r42 = {main=rtrn (lit 42)}
