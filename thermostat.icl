module thermostat

import StdEnv
import Data.Tuple

import iTasks

import mTask.Interpret
import mTask.Show

Start w = doTasks main w

main = enterDevice
	>>! \spec->withDevice spec \dev->
	   withShared (18.0, 20.0) \targets->
	   withShared 19.0 \temp->
			    (updateSharedInformation [] targets <<@ Title "Goal")
			||- (viewSharedInformation [] temp <<@ Title "Current")
			||- liftmTask (mTask targets temp) dev

mTask :: (Shared sds1 (Real, Real)) (Shared sds2 Real) -> Main (MTask v Real) | mtask, liftsds, dht v & RWShared sds1 & RWShared sds2
mTask goalShare tempShare =
	declarePin D3 PMOutput \fanPin->
	declarePin D4 PMOutput \heatPin->
	DHT (DHT_DHT (DigitalPin D2) DHT22) \dht->
	   liftsds \upper = mapReadWrite (fst, \r (w, _)-> ?Just (r, w)) ?None goalShare
	In liftsds \down  = mapReadWrite (snd, \r (_, w)-> ?Just (w, r)) ?None goalShare
	In liftsds \temp  = tempShare
	In fun \tread=(
		\ot->    temperature dht
			>>*. [IfValue ((!=.)ot) (setSds temp)]
			>>=. tread
	) In fun \heater=(monitor heater (<.) temp down fanPin
	) In fun \cooler=(monitor cooler (>.) temp upper heatPin
	) In {main
		=    writeD fanPin true >>|. writeD heatPin true
		>>|. tread (lit 0.0)
		.||. heater false
		.||. cooler false
	}
where
	monitor rec op temp down pin on =
		getSds temp .&&. getSds down
			>>*. [ IfValue (tupopen \(temp, down)->op temp down &. on)
						\_->writeD pin true
			     , IfValue (tupopen \(temp, down)->Not (op temp down) &. Not on)
						\_->writeD pin false]
			>>=. rec o Not
