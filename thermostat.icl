module thermostat

import StdEnv
import Data.GenHash
import Data.Tuple

import iTasks

import mTask.Interpret
import mTask.Interpret.Devices
import mTask.Show

Start w = doTasks main w

main :: Task ()
main = enterInformation [] <<@ Title "Device details"
	>>? \dd=:{TCPSettings|host,port}->withDevice dd \dev->
	   withShared (18.0, 20.0) \targets->
	   withShared 19.0 \temp->
			    (updateSharedInformation [] targets <<@ Title "Goal")
			||- (viewSharedInformation [] temp <<@ Title "Current")
			||- liftmTask (mTask targets temp) dev
	>>* [OnAction (Action "Stop") (always (return ()))]

mTask :: (Shared sds1 (Real, Real)) (Shared sds2 Real) -> Main (MTask v Real) | mtask, lowerSds, dht v & RWShared sds1 & RWShared sds2 & TC (sds1 () Real Real) & TC (sds2 () Real Real)
mTask goalShare tempShare =
	declarePin D3 PMOutput \fanPin->
	declarePin D4 PMOutput \heatPin->
	dht (DHT_DHT (DigitalPin D2) DHT22) \dht->
	   lowerSds \upper = mapReadWrite (fst, \r (w, _)-> ?Just (r, w)) ?None goalShare
	In lowerSds \down  = mapReadWrite (snd, \r (_, w)-> ?Just (w, r)) ?None goalShare
	In lowerSds \temp  = tempShare
	In fun \tread=(
		\ot->    temperature dht
			>>*. [IfValue ((!=.)ot) (setSds temp)]
			>>=. tread
	) In fun \heater=(monitor heater (<.) temp down fanPin
	) In fun \cooler=(monitor cooler (>.) temp upper heatPin
	) In {main
		=    writeD fanPin true >>|. writeD heatPin true
		>>|. tread (lit 0.0)
		.||. heater false
		.||. cooler false
	}
where
	monitor rec op temp down pin on =
		getSds temp .&&. getSds down
			>>*. [ IfValue (tupopen \(temp, down)->op temp down &. on)
						\_->writeD pin true
			     , IfValue (tupopen \(temp, down)->Not (op temp down) &. Not on)
						\_->writeD pin false]
			>>=. rec o Not
