module switch

import StdEnv, iTasks

import mTask.Interpret
import mTask.Interpret.Devices

Start w = doTasks main w

main :: Task ()
main = enterInformation [] <<@ Title "Device details"
	>>? \dd=:{TCPSettings|host,port}->withDevice dd \dev->
		liftmTask switch dev
	>>* [OnAction (Action "Stop") (always (return ()))]

switch :: Main (MTask v Bool) | mtask v
switch
	= declarePin D1 PMInput \d1->
		declarePin D4 PMOutput \d4->
		fun \switch=(\x->
			(writeD d4 x)
			>>|. delay (lit 50) // Debounce
			>>|. interrupt falling d1
			>>|. switch (Not x)
		)
		In {main=switch (lit False)}
