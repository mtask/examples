module switch

import StdEnv, iTasks

import mTask.Interpret
import mTask.Interpret.Devices

Start w = doTasks main w

main :: Task Bool
main = enterInformation []
	>>? \spec=:{TCPSettings|host,port}->withDevice spec
		\dev->liftmTask switch dev

switch :: Main (MTask v Bool) | mtask v
switch
	= declarePin D1 PMInput \d1->
		declarePin D4 PMOutput \d4->
		fun \switch=(\x->
			(writeD d4 x)
			>>|. delay (lit 50) // Debounce
			>>|. interrupt Falling d1
			>>|. switch (Not x)
		)
		In {main=switch (lit False)}

