module motionDetector

/**
 * This program is a motion detector for demonstrating interrupts.
 *
 * Connect the PIR to D2
 * Builtin LED is assumed to be D9
 *
 * If motion is detected: turn on the LED for two seconds
 * Every minute, turn on the LED for half a second to signal that it runs.
 */

import StdEnv, iTasks
import Data.Func

import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

main :: Task Bool
main = enterInformation []
	>>? \spec=:{TCPSettings|host,port}->withDevice spec
		\dev->liftmTask motionDetector dev

motionDetector :: Main (MTask v Bool) | mtask v
motionDetector
	= PIR D2 \pir->
	// Builtin LED is D4 on the D1 Wemos Mini
	// Builtin LED is D9 on the Firebeetle 32
	  declarePin D9 PMOutput \lpin->
	fun \ping=(\()->
		     writeD lpin true
		>>|. delay (lit 500)
		>>|. writeD lpin false
		>>|. delay (lit 60000)
		>>|. ping ()
	) In 
	let mot = rpeat (interrupt rising pir >>|. writeD lpin true >>|. delay (lit 2000) >>|. writeD lpin false)
	in {main=mot .||. ping ()}
