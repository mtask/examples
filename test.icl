module test

import StdEnv
from StdFunc import seq
import Data.Either
import Data.Func
import Data.List => qualified group
import Data.Tuple
import Data.Tree
import Control.Applicative
import Text
import Text.HTML

import mTask.Language
import mTask.Show
import mTask.Simulate
import mTask.Interpret
import mTask.Interpret.String255
import mTask.Interpret.Instructions
import mTask.Interpret.Devices
import CleanSerial, CleanSerial.iTasks
//import mTask.Interpret.Device.Simulator

import qualified Data.Map as DM

import mTask.Interpret.Compile
import mTask.Interpret
//import Examples

import Text.GenPrint
import iTasks

derive class iTask CompileOpts, ChoiceNode

Start w = flip doTasks w $
	(parallel [(Embedded, tune (Title "Add device") o addDevice)] [] <<@ ArrangeWithTabs False)
	>>* [OnAction (Action "Shutdown") $ always $ shutDown 0]
where
	addDevice :: (SharedTaskList ()) -> Task ()
	addDevice stl
		 =   (updateInformation [] "name" <<@ Label "Device name")
		-&&- enterDevice
		>>! \(name, dev)->appendTask Embedded
			(\_->tune (Title name) $ catchAll
					(withDevice dev manageDevice)
					(\e->viewInformation [] e @! [])
			>>* [OnAction ActionClose $ always $ treturn ()]
		) stl
		>-| addDevice stl


//manageDevice :: MTDevice -> Task (Int, String)
manageDevice dev
	= parallel [(Embedded, tune (Title "Pick a task") o forever o taskpicker)] []
	<<@ ArrangeWithTabs False
where
	taskpicker :: (SharedTaskList ()) -> Task ()
	taskpicker stl = withShared [] \sel->
			editSharedSelection
				[SelectInTree (flip seq [] o map toChoiceTree) (\y x->[x\\x<-x|x>=0])]
				[(i, split "/" n)\\i<-[0..] & (n,_)<-tasks] sel
		||- forever (
				watch sel >>* [OnValue $ ifValue (not o isEmpty) \idx->
					let (name, task) = tasks !! hd idx in 
						appendTask Embedded (\_->
							tune (Title name) $ task dev
							>>* [OnAction ActionClose $ always $ treturn ()]
						) stl
					>-| set [] sel @! ()
				]
			)

	toChoiceTree :: (Int, [String]) [ChoiceNode] -> [ChoiceNode]
	toChoiceTree (i, []) cs = cs
	toChoiceTree (i, [n:ns]) [] = [
		{ id=if (ns =: []) i (-1-i), label=n, icon= ?None, expanded=False
		, children=toChoiceTree (i, ns) []}]
	toChoiceTree (i, [n:ns]) [c=:{label,children}:cs]
		| label == n
			= [{ChoiceNode | c & children=toChoiceTree (i, ns) children}:cs]
		= [c:toChoiceTree (i, [n:ns]) cs]

runTask :: (Main (BCInterpret (TaskValue b))) MTDevice -> Task [(Int, TaskValue b)] | iTask, type b
runTask task dev
	= parallel
		[(Embedded, \stl->appendTask Embedded (\_->liftmTask task dev) stl
			>>- \tid->viewSharedInformation [] (sdsFocus (Right tid) (taskListItemValue stl)) <<@ Title "Task Value"
			@? \tv->case tv of
				Value (Value t s1) s2 = Value t (s1 && s2)
				_ = NoValue)
		,(Embedded, \stl->updateInformation [] zero <<@ Title "Compilation settings"
			>&> \sh->whileUnchanged sh
			    \v ->case v of
			?None = (viewInformation [] () <<@ Title "No settings") @? const NoValue
			?Just opts
				# (taskwidth, shares, hardware, instructions) = compileOpts opts task
				= (viewInformation []
					(formatDebugInstructions $ debugInstructions instructions) <<@ Title "Bytecode")
				-|| (sequence (map snd shares) >>- viewInformation [ViewAs $ map
					\v->{v & bcs_value=fromString (safePrint v.bcs_value)}])
				-|| allTasks [viewSharedInformation [ViewAs $ safePrint] sh\\(?Just sh, _)<-shares]
				@? const NoValue)
		]
		[] <<@ ArrangeHorizontal

listItem :: String (A.v: b -> Main (MTask v a) | mtask, dht, LEDMatrix, LightSensor, AirQualitySensor v) -> (String, MTDevice -> Task ()) | iTask b & type a
listItem descr mt
# pp = mt
# sim = mt
= ( descr
	, \dev->tune (Title descr)
		$   enterInformation []
		>&^ viewSharedInformation [ViewAs $ concat o maybe [] (showMain o pp)]
		>>* [ OnAction (Action "Run") $ hasValue \a->runTask (mt a) dev >>*
				[ OnAction (Action "Step on value") $ withValue $ ?Just o treturn o Right
				, OnValue $ ifStable $ treturn o Right
					, OnException \e->case e of
					e=:MTERTSError = throw e
					e = treturn (Left e)
				] >>- viewInformation [] @! ()
			, OnAction (Action "Simulate") $ hasValue \a->simulate (sim a) @! ()
			]
	)

tasks :: [(String, MTDevice -> Task ())]
tasks =
	map (appFst $ (+++)"Return constant values/")
		[ listItem "Return unit"
			\()->{main=rtrn (lit ())}
		, listItem "Return an integer"
			\i->{main=rtrn (int (lit i))}
		, listItem "Return a long integer"
			\i->{main=rtrn (long (lit i))}
		, listItem "Return a real"
			\i->{main=rtrn (real (lit i))}
		, listItem "Emit an unstable integer (won't return)"
			\i->{main=unstable (int (lit i))}
		, listItem "Emit an unstable long (won't return)"
			\i->{main=unstable (long (lit i))}
		, listItem "Emit an unstable real (won't return)"
			\i->{main=unstable (real (lit i))}
		, listItem "Add two reals"
			\(i, j)->{main=rtrn (real (lit i) +. real (lit j))}
		] ++
	map (appFst $ (+++)"Exceptions/")
		[ listItem "Stack overflow"
			\()->
				fun \foo=(\()->lit 0 +. foo ())
				In {main=rtrn (foo ())}
		, listItem "Stack overflow (2)"
			\()->
				fun \foo=(\()->lit 0 +. foo ())
				In {main=delay (lit 100) >>=. \_->rtrn (foo ())}
		, listItem "Out of memory"
			\()->
				let t  = (Long 42, Long 38)
				    t2 = (t, t)
				    t3 = (t2, t2)
				    t4 = (t3, t3)
				    t5 = (t4, t4)
				in {main=rtrn (lit t5) .&&. rtrn (lit t5) .&&. rtrn (lit t5)}
		, listItem "Floating point exception"
			\()->{main=rtrn (lit 1 /. lit 0)}
		] ++
	map (appFst $ (+++)"Functions/")
		[ listItem "Factorial with an integer"
			\i->
				fun \fac=(\i =
					If (i ==. lit 0) (lit 1) (i *. fac (i -. lit 1))
				) In {main=rtrn (fac (lit i))}
		, listItem "Factorial with a long"
			\i->
				fun \fac=(\i =
					If (i ==. lit zero) (lit one)
						(i *. fac (i -. lit one))
				) In {main=rtrn (fac (lit (Long i)))}
		, listItem "Factorial with an integer tail call optimized"
			\i->
				fun \fac=(\(i, acc)=
					If (i ==. lit zero) acc (fac (i -. lit 1, acc *. i)))
				In {main=rtrn (fac (lit i, lit one))}
		, listItem "Factorial with an long tail call optimized"
			\i->
				fun \fac=(\(i, acc)=
					If (i ==. lit zero) acc (fac (i -. lit one, acc *. i)))
				In {main=rtrn (fac (lit (Long i), lit one))}
		, listItem "Nth Fibonacci number"
			\i->
				fun \fib=(\i->If (i ==. lit 0) (lit 0)
					$ If (i ==. lit 1) (lit 1)
					$ fib (i -. lit 1) +. fib (i -. lit 2))
				In {main=rtrn (fib (lit i))}
		, listItem "Nth Fibonacci number tail call optimized"
			\i->
				fun \fib=(\(n, p, pp)->If (n ==. lit 0) p
					$ fib (n -. lit 1, p +. pp, p))
				In {main=rtrn (fib (lit i, lit 0, lit 1))}
		, listItem "Count down"
			\i->
				fun \count=(\x->If (x <=. lit 0) (lit 0) (count (x -. lit 1)))
				In {main=rtrn (count (int (lit i)))}
		] ++
	map (appFst $ (+++)"Traditional benchmarks/")
		[ listItem "acker (3,2) is about the highest you can get I think"
			\(i,j)->
				fun \acker=(\(i,j) =
					If (i ==. lit 0) (j +. lit 1) $
					If (j ==. lit 0) (acker (i -. lit 1, lit 1)) $
						acker (i -. lit 1, acker (i, j -. lit 1))
				) In {main=rtrn (acker (lit i, lit j))}
		, listItem "nfib"
			\i->
				fun \nfib=(\n =
					If (n <. lit 2) (lit 1) $
						nfib (n -. lit 1) +. nfib (n -. lit 2) +. lit 1
				) In {main=rtrn (nfib (lit i))}
		, listItem "rfib"
			\i->
				fun \rfib=(\n =
					If (n <. lit 1.5) (lit 1.0) $
						rfib (n -. lit 1.0) +. rfib (n -. lit 2.0) +. lit 1.0
				) In {main=rtrn (rfib (lit i))}
		, listItem "tak"
			\(x,y,z)->
				fun \tak=(\(x,y,z) =
					If (x <=. y) z $ tak
						( tak (x -. lit 1, y, z)
						, tak (y -. lit 1, z, x)
						, tak (z -. lit 1, x, y))
				) In {main=rtrn (tak (lit x, lit y, lit z))}
		] ++
	map (appFst $ (+++)"Parallel/")
		[ listItem "Return the left part of a disjuncton of integers"
			\(a, b)->{main=rtrn (int (lit a)) .||. unstable (int (lit b))}
		, listItem "Return the right part of a disjuncton of integers"
			\(a, b)->{main=unstable (int (lit a)) .||. rtrn (int (lit b))}
		, listItem "Return the first item of a tuple of integers"
			\(i, j)->{main=rtrn (first (lit (cast 0 i, cast 0 j)))}
		, listItem "Return the second item of a tuple of integers"
			\(i, j)->{main=rtrn (second (lit (cast 0 i, cast 0 j)))}
		, listItem "Return the a tuple of integers"
			\(i, j)->{main=rtrn (lit (cast 0 i, cast 0 j))}
		, listItem "Return the conjunction of two integers"
			\(i, j)->{main=rtrn (int (lit i)) .&&. rtrn (int (lit j))}
		, listItem "Return a huge tuple of integers"
			\(a, b, c, d, e)->{main=rtrn (int (lit a)) .&&. rtrn (int (lit b)) .&&. rtrn (int (lit c)) .&&. rtrn (int (lit d)) .&&. rtrn (int (lit e))}
		, listItem "Return a huge tuple of longs"
			\(a, b, c)->{main=rtrn (long (lit a)) .&&. rtrn (long (lit b)) .&&. rtrn (long (lit c))}
		] ++
	map (appFst $ (+++)"GPIO/")
		[ listItem "Read an analog pin (won't return)"
			\p->{main=readA (lit p)}
		, listItem "Read a digital pin (won't return)"
			\p->{main=readD (lit (hd [p,D0]))}
		, listItem "Write an analog pin"
			\(v, p)->declarePin p PMInput \p->{main=writeA p (lit v)}
		, listItem "Write a digital pin"
			\(v, p)->declarePin (hd [p,D0]) PMOutput \p->{main=writeD p (lit v)}
		] ++
	map (appFst $ (+++)"Step/")
		[ listItem "Return unit after a step"
			\i->{main=rtrn (int (lit i)) >>=. \_->rtrn (lit ())}
		, listItem "Return an integer through a step"
			\i->{main=rtrn (int (lit i)) >>=. rtrn}
		, listItem "Return a long through a step"
			\i->{main=rtrn (long (lit i)) >>=. rtrn}
		, listItem "Return a real through a step"
			\i->{main=rtrn (real (lit i)) >>=. rtrn}
		, listItem "Return a tuple of ints through a step"
			\(i,j)->{main=rtrn (lit (cast 0 i, cast 0 j)) >>=. rtrn}
		, listItem "Return a tuple of ints through a step and return the first"
			\(i,j)->{main=rtrn (lit (cast 0 i, cast 0 j)) >>=. rtrn o first}
		, listItem "Return a tuple of ints through a step and return the second"
			\(i,j)->{main=rtrn (lit (cast 0 i, cast 0 j)) >>=. rtrn o second}
		, listItem "Return a tuple of ints through two steps"
			\(i,j)->{main=rtrn (int (lit i)) >>=. \i->rtrn (int (lit j)) >>=. \j->rtrn (tupl i j)}
		, listItem "Return a tuple of longs through two steps"
			\(i,j)->{main=rtrn (long (lit i)) >>=. \i->rtrn (long (lit j)) >>=. \j->rtrn (tupl i j)}
		] ++
	map (appFst $ (+++)"Time/")
		[ listItem "Return an integer forever (won't return)"
			\i->{main=rpeat (rtrn (int (lit i)))}
		, listItem "Return an integer after 5 seconds"
			\i->{main=delay (lit 5000) >>|. rtrn (int (lit i))}
		, listItem "Wait a specified amount of time"
			\i->{main=delay (int (lit i))}
		] ++
	map (appFst $ (+++)"Peripherals/")
		[ listItem "Read out a DHT (won't return)"
			\p->DHT p \dht->{main=temperature dht .&&. humidity dht}
		, listItem "OLED buttons"
			\()->{main=readD d3 .&&. readD d4}
		, listItem "Read out the PIR"
			\()->declarePin D3 PMInput \d3->{main=readD d3}
		, listItem "Read out the PIR as a Peripheral"
			\()->PIR D3 \pir->{main=motion pir}
		, listItem "Light Sensor"
			\()->lightsensor (i2c 0x23) \ls->{main=light ls}
		, listItem "Air Quality TVOC && CO2"
			\()->airqualitysensor (i2c 0x5B) \aqs->{main=tvoc aqs .&&. co2 aqs}
		, listItem "Air Quality TVOC && CO2 with temperature"
			\p->DHT p \dht->airqualitysensor (i2c 0x5B) \aqs->{main=setEnvFromDHT aqs dht >>|. tvoc aqs .&&. co2 aqs}
		, listItem "Sound Detector"
			\()->soundDetector D0 A0 \ss->{main=soundPresence ss .&&. soundLevel ss}
		] ++
	map (appFst $ (+++)"mTasks/")
		[ listItem "Blink a led using a function"
			\pin->
				fun \blink=(
					     \st->writeD (dpin (lit pin)) st
					>>=. \_-> delay (lit 1000)
					>>|.      blink (Not st))
				In {main = voidTask (blink true)}
		, listItem "Blink a led using rpeat"
				\pin->
					{main=rpeat (
						     delay (lit 1000)
						>>|. readD (dpin (lit pin))
						>>~. writeD (dpin (lit pin)) o Not)
					}
		, listItem "Set an LED a the LED matrix"
				\(x, y, z)->
					ledmatrix {clockPin=DigitalPin D7,dataPin=DigitalPin D5} \lm->
					{main=LMDot lm (lit x) (lit y) (lit z)
						>>|. LMDisplay lm
					}
		, listItem "Bounce a led ball"
				\()->
					ledmatrix {clockPin=DigitalPin D7,dataPin=DigitalPin D5} \lm->
					fun \bounce=(\(x, dx)->
						If (x +. dx >. lit 7 |. x +. dx <. lit 0)
							(bounce (x, dx *. lit -1))
							(	     LMDot lm x (lit 3) (lit False)
								>>|. LMDot lm (x +. dx) (lit 3) (lit True)
								>>|. LMDisplay lm
								>>|. delay (lit 100)
								>>=. \_->bounce (x +. dx, dx)
							)
					) In
					{main=voidTask (bounce (lit 0, lit 1))}
		, listItem "Bounce with shares"
				\(ix, iy)->
					ledmatrix {clockPin=DigitalPin D7,dataPin=DigitalPin D5} \lm->
					sds \sx=abs ix rem 7 In
					sds \sdx=1 In
					sds \sy=abs iy rem 7 In
					sds \sdy=1 In
					{main=rpeat (
						          getSds sx
						>>~. \x-> getSds sdx
						>>~. \dx->getSds sy
						>>~. \y-> getSds sdy
						>>~. \dy->LMDot lm x y (lit False)
						>>=. \_-> If (x +. dx >. lit 7 |. x +. dx <. lit 0)
								(setSds sdx (dx *. lit -1))
								(If (y +. dy >. lit 7 |. y +. dy <. lit 0)
									(setSds sdy (dy *. lit -1))
									(        setSds sx (x +. dx)
									>>=. \_->setSds sy (y +. dy)
									>>=. \_->LMDot lm (x +. dx) (y +. dy) (lit True)
									>>=. \_->LMDisplay lm
									>>=. \_->delay (lit 100)
									>>|. rtrn (lit 100)
									)
								)
					)}
		] ++
	//Examples from sjoerds thesis
	map (appFst $ (+++)"energy/")
		[ listItem "Measure the temperature every 60 seconds"
			\dhtinfo->
				DHT dhtinfo \dht->
				{main = temperature` (BeforeSec (lit 60)) dht}
		, listItem "Light switch using interrupts (button pin, led pin)"
			\(bpin, lpin)->
				declarePin bpin PMInput \button->
				declarePin lpin PMOutput \led->
				fun \switch=(\x->
					     writeD (dpin led) x
					>>|. delay (lit 50) // Debounce
					>>|. interrupt falling (dpin button)
					>>|. switch (Not x)
				) In {main=voidTask (switch (lit False))}
		, listItem "Light switch using polling (button pin, led pin)"
			\(bpin, lpin)->
				declarePin bpin PMInput \button->
				declarePin lpin PMOutput \led->
				fun \switch=(\x->
					     writeD (dpin led) x
					>>|. delay (lit 50) // Debounce
					>>|. readD (dpin button)
					>>*. [IfValue Not (\_.switch (Not x))]
				) In {main=voidTask (switch (lit False))}
		, listItem "Plant monitor (moisture sensor's analog pin, light sensor i2c)"
			\(mpin, ls)->
				declarePin mpin PMInput \m->
				lightsensor ls \l->
				let moistureSensor = readA` (BeforeSec (lit 300)) m >>*. [IfValue ((<=.) (lit 100)) (\_. rtrn (lit True))]
				in let lightSensor = light` (BeforeSec (lit 90)) l >>*. [IfValue ((<=.) (lit 150.0)) (\_. rtrn (lit True))]
				in {main = lightSensor .||. moistureSensor}
		] ++
	map (appFst $ (+++)"Workflow Pattern (v.d. Aalst)/")
		(flatten [
			map (appFst $ (+++)"Control Flow/")
				[ listItem "Sequence"
					\()->declarePin D0 PMOutput \p->{main=
						     writeD p (lit True)
						>>|. writeD p (lit False)
					}
//				, listItem "Parallel split"
//					\()->declarePin D0 PMInput \p->
//					     declarePin D1 PMOutput \p1->{main=
//						     writeD p (lit True) p0
//						>>|. (reaD[ IfValue id $ writeD p1
//						     , IfValue not $ writeD p1
//						     ]
//					}
				]
		])

cast :: a a -> a
cast _ a = a

int :: (v Int) -> v Int
int i = i

real :: (v Real) -> v Real
real i = i

long :: (v Long) -> v Long
long i = i

dpin :: (v DPin) -> v DPin
dpin i = i

apin :: (v APin) -> v APin
apin i = i

voidTask :: (MTask v ()) -> MTask v ()
voidTask i = i

intShare = sharedStore "intShare" 42
