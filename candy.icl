module candy

import StdEnv, iTasks
import Text

import mTask.Show
import mTask.Interpret
import mTask.Interpret.Devices

Start :: !*World -> *World
Start w = doTasks main w

main :: Task ()
main = enterInformation [] <<@ Title "Device details"
	>>? \dd=:{TCPSettings|host,port}->withDevice dd \dev->
		    updateInformation [] [GLeft,GRight] <<@ Title "Gesture Sequence"
		>&> withSelection (viewInformation [] "Invalid gesture sequence")
		\gseq->liftmTask (candy gseq) dev
			-|| (viewInformation [] (concat (showMain (candy gseq))))
					<<@ Title "mTask code"
	>>* [OnAction (Action "Stop") (always (return ()))]
where
	candy :: [Gesture] -> Main (MTask v Bool) | mtask, GestureSensor v
	candy gseq =
		gestureSensor gestureSensorPAJ7620Breakout \ges->
		fun \rges=(\()->recogGes rges ges GNone gseq)
		In {main=writeD d4 true >>|. rges ()}
	where
		recogGes :: (() -> MTask v Bool) (v GestureSensor) Gesture [Gesture]
			-> MTask v Bool | mtask, GestureSensor v
		recogGes rges ges prev []
			= writeD d4 false >>|. delay (lit 500) >>|. writeD d4 true >>|. rges ()
			// Motor simulation
		recogGes rges ges prev [g:gs] = gesture ges >>*.
			[IfValue ((==.) (lit g)) \_->recogGes rges ges g gs
			,IfValue (\x->x !=. lit GNone &. x !=. lit prev) \_->rges ()
			]
