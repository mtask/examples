module blink

import iTasks.Extensions.DateTime
import StdEnv, iTasks

import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

main :: Task Bool
main = enterInformation []
	>>? \spec=:{TCPSettings|host,port}->withDevice spec
		\dev->waitForTimer True 60 @! True//liftmTask blink dev
//		\dev->liftmTask blink dev

blink :: Main (MTask v Bool) | mtask v
blink
	// Builtin LED is D4 on the D1 Wemos Mini
	= declarePin D4 PMOutput \d4->
	fun \blink=(\x->
		     delay (lit 500)
		>>|. writeD d4 x
		>>=. blink o Not)
	In {main=blink (lit True)}
