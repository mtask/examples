module blink

import iTasks.Extensions.DateTime
import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

main :: Task ()
main = enterInformation [] <<@ Title "Device details"
	>>? \dd=:{TCPSettings|host,port}->withDevice dd \dev->
		liftmTask blinkTask dev
	>>* [OnAction (Action "Stop") (always (return ()))]
where
	blinkTask :: Main (MTask v Bool) | mtask v
	blinkTask = declarePin D13 PMOutput \ledPin->
		fun \blink=(\st->
			     writeD ledPin st
			>>|. delay (lit 500)
			>>|. blink (Not st)
		) In {main=blink true}
