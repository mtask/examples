module temp

/*
 * Run the server with
 * ./temp
 *
 * Run the mTask client
 * ./client
 *
 * Go to the website of the server and enter IPOFCLIENT:8123 for the client
 */

import Data.GenHash
import StdEnv
import iTasks
import iTasks.Extensions.DateTime

import mTask.Language
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

tempSDS :: SimpleSDSLens [(DateTime, Real)]
tempSDS = sharedStore "temperatures" [({DateTime|year=0,mon=0,day=0,hour=0,min=0,sec=0}, 0.0)]

latestTemp :: SimpleSDSLens (DateTime, Real)
latestTemp = mapReadWrite (hd, \x xs-> ?Just [x:xs]) ?None tempSDS

main :: Task ()
main = enterInformation [] <<@ Title "Device details"
	>>? \dd=:{TCPSettings|host,port}->withDevice dd \dev->
		liftmTask devTask dev
		-|| viewSharedInformation [ViewAs view] tempSDS
			<<@ Title "Current Temperature (°C)"
	>>* [OnAction (Action "Stop") (always (return ()))]
where
	view :: ([a] -> ?a)
	view = listToMaybe

	devTask :: Main (MTask v ()) | mtask, dht, lowerSds v
	devTask = dht (DHT_DHT (DigitalPin D4) DHT11) \dht =
		lowerSds \localSDS =
			mapRead snd (dateTimeStampedShare latestTemp)
		In fun \temp = ( \oldtemp->
			     temperature dht
			>>*. [IfValue ((!=.) oldtemp) (setSds localSDS)]
			>>=. temp)
		In {main = temp (lit 0.0)}
