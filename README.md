# The mTask example programs

For general mTask information please refer to the main repository:
https://gitlab.science.ru.nl/mtask/mtask

Automatically built packages can be found here:
[zip](https://gitlab.science.ru.nl/mtask/examples/builds/artifacts/master/raw/mtask-examples-windows-x64.zip?job=windows-x64),
[tar.gz](https://gitlab.science.ru.nl/mtask/examples/builds/artifacts/master/raw/mtask-linux-x64.tar.gz?job=linux-x64).
