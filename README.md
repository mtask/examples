# The mTask example programs

For general mTask information please refer to the server repository:

https://gitlab.com/mtask/server

## Run an example

To run the examples, install [nitrile](clean-lang.org) and call:

```
nitrile update
nitrile fetch
nitrile build
```

You can run the client from the `nitrile-packages` by running:

`nitrile run mtask-client-desktop mtask-client-tty`

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`mtask-examples` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
