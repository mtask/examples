#!/bin/bash
set -e

if [ $# -eq 1 ]
then
	if [ $1 = "-w" ]
	then
		WINDOWS=true
	elif [ $1 = "-l" ]
	then
		WINDOWS=
	else
		echo "Usage: $0 [-l|-w]"
		exit 1
	fi
fi

TARGET=${TARGET:-mtask-examples}

rm -Irf "$TARGET"
mkdir -p "$TARGET"/examples/mTask

cp -R *.[id]cl "$TARGET"/examples/mTask

if [ -z $WINDOWS ]
then
	tar -czf mtask-examples-linux-x64.tar.gz "$TARGET"
else
	zip -r mtask-examples-windows-x64.zip "$TARGET"
fi
